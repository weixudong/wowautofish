from PIL import ImageGrab
import cv2
import numpy as np
import pyaudio
import pyautogui
import time
import random
import math

#
curColor=(73,15,14)
diffColor=15
def ignoreView(x,y):
    if(x>=628 and y < 240):
        return False
    if x>=700:
        return False

    if(x>=0 and x <= 24 and y >=474 and y<=700):
        return False
    if y>600:
        return False
    if x<236 and y <=182:
        return False
    return True
def findCursor():
    s_found=0
    X,Y=-1,-1
    bbox = (0, 0, 800, 690)
    im = ImageGrab.grab(bbox)
    pix = im.load()
    width = im.size[0]
    height = im.size[1]
    for y in range(120,height,1):
        if s_found == 1:
            break
        
        for x in range(0,width,1): 
            if ignoreView(x,y)==False:
                continue
            r, g, b ,a= pix[x, y]
            if math.fabs(curColor[0]-r)<=diffColor and math.fabs(curColor[1]-g)<=diffColor and math.fabs(curColor[2]-b)<=diffColor:
                X=x
                Y=y
                s_found=1
                break
    
    return (s_found,X,Y)

def food(): 
    count = 0
    start = time.time()
    while (count<5000):
        time.sleep(4)
        count+=1
        use_time = (time.time()-start)/1000
        print("food:{0}-{1}".format( count,use_time))
        #time.sleep(1)
        #
        if( count>=6):
            pyautogui.press('space')
            time.sleep(1)
            pyautogui.press('4')
            time.sleep(30)
            count=0

        pyautogui.press('space')
        time.sleep(2)
        pyautogui.press('2')
        #time.sleep(1)
        

def main():
    #time.sleep(3)
    count = 0
    start = time.time()
    while (count<5000):
        time.sleep(4)
        count+=1
        use_time = (time.time()-start)/1000
        print("fish:{0}-{1}".format(count,use_time))
        time.sleep(1)
        #
        if( random.random()<0.3):
            pyautogui.press('space')
            time.sleep(1)
        pyautogui.press('1')
        time.sleep(3)
        s_found,X,Y = findCursor()
        if s_found==0:
            continue
        print (s_found,X,Y)
        screenWidth, screenHeight = pyautogui.size()
        pyautogui.moveTo(X/2, Y/2)

        detectSound()

        #pyautogui.keyDown('shiftleft')
        pyautogui.click(button='right')
        time.sleep(1)
        #pyautogui.keyUp('shiftleft')
        pyautogui.click(button='left')
        time.sleep(0.5)
        pyautogui.click(button='left')
        


     
    
    #im.save('as.png')



def find_device(device_sought, device_list):
    for device in device_list:
        if device["name"] == device_sought:
            return device["index"]
    raise KeyError("Device {} not found.".format(device_sought))

def get_device_list(p):
    num_devices = p.get_device_count()
    device_list = [p.get_device_info_by_index(i) for i in range(0, num_devices)]
    return device_list

def max_count(arr,score):
    ret=0
    for i in range(len(arr)):
        if( arr[i]>score):
            ret+=1
    return ret

def get_soundflower_index(p):
    device_list = get_device_list(p)
    soundflower_index = find_device("Soundflower (2ch)", device_list)
    return soundflower_index

def detectSound():
    


    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    CHUNK = int(1024)
    RECORD_SECONDS = 30

    p = pyaudio.PyAudio()


    device_index = get_soundflower_index(p)
    print(device_index)
    stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                input_device_index=device_index,
                frames_per_buffer=CHUNK)

    print("* recording")

    #buf = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        wave_data = np.fromstring(data, dtype=np.short)
        wave_data.shape = -1, 2
        wave_data = wave_data.T
        #left = wave_data[0]

        
        if (max_count(wave_data[0],50)>10):
            break
        # zcr = sum(left[0:CHUNK-1] * left[1:CHUNK] < 0)/(CHUNK-1)
        # if len(buf) == 7:
        #     if np.mean(buf)>0.4 and np.std(buf) < 0.2:
        #         break
                
        #     buf.pop(0)
        # buf.append(zcr)

    
    print('Got a Bite!')

    stream.stop_stream()
    stream.close()
    p.terminate()

def test():
    img = cv2.imread('as.png', 0)
    template = cv2.imread('point.png', 0)
    h, w = template.shape[:2]  # rows->h, cols->w
    h2, w2 = img.shape[:2]  # rows->h, cols->w
    res = cv2.matchTemplate(img, template, cv2.TM_CCORR_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    left_top = max_loc 
    right_bottom = (left_top[0] + w, left_top[1] + h)  # 
    cv2.rectangle(img, left_top, right_bottom, 255, 2)  # 
    #print(left_top)
    print(max_loc)
    #print(right_bottom)
    cv2.imwrite("as2.jpg", img)

main()

#food()

#detectSound()